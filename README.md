[![pipeline status](https://gitlab.com/rawsec/rawsec-cybersecurity-list/badges/master/pipeline.svg)](https://gitlab.com/rawsec/rawsec-cybersecurity-list/commits/master)
[![GitHub license](https://img.shields.io/github/license/noraj/rawsec-cybersecurity-inventory.svg)](https://gitlab.com/rawsec/rawsec-cybersecurity-list/blob/master/LICENSE)
[![Discord](https://img.shields.io/discord/437247125508587540.svg?style=flat&logo=discord)](https://discord.gg/Wspwv2h)

# Rawsec's Cybersecurity Inventory

Name            | Link
---             | ---
Website         | [link](https://inventory.rawsec.ml/)
Documentation   | [link](https://inventory.rawsec.ml/docs/)
Git repository  | [link](https://gitlab.com/rawsec/rawsec-cybersecurity-list)
Merge Requests  | [link](https://gitlab.com/rawsec/rawsec-cybersecurity-list/merge_requests)
Issues          | [link](https://gitlab.com/rawsec/rawsec-cybersecurity-list/issues)
Wiki            | [link](https://gitlab.com/rawsec/rawsec-cybersecurity-list/wikis/home)
Chat            | [link](https://discord.gg/Wspwv2h)

For contribution, development, usage or questions check the documentation.

### About

See [About](https://inventory.rawsec.ml/about.html).
